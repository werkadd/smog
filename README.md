# Smog Analysis

This is a project for Advanced Programming in Python at AGH. It is a tool for analyzing some datasets related to air pollution in Poland and around the world.

## Setup

Please install the required modules:

    pip install -r requirements.txt

## Run

Navigate to root "smog/" directory, and run:

    python main.py

## Folders

**data/** - Data files for analysis.

**tools/** - Module with the source code used in the application.

**original/** - Original jupyter notebooks adapted for the application.

**images/** - Images for the application and README.