# # [Original Introduction]
# 
# The following notebook presents an exploratory data analysis that aims to find a corellation between deaths caused by various types of diseases and air pollution, particularrily the PM2.5 and PM10. It goes through several stages of the data analysis process, from loading and parsing the data, through cleaning it, and finishes on joining the relevant dataseries and visulaizing the relationship between variables on a series of scatter plots.
# 
# # Data used in this study
# We use Eurostat as the datasource in our study.
#     
#     Air pollution data come from https://ec.europa.eu/eurostat/databrowser/view/sdg_11_50/default/table?lang=en
#     and contain average yearly concentration of PM2.5 and PM10 in most of the european countries. The data were collected in the years 2000-2017 for PM10 and 2005-2017 for PM2.5
#     
#     Death by disease data come ftom https://ec.europa.eu/eurostat/web/sdi/good-health-and-well-being and contain records for various diseases as cause of death. It contains data from 2000-2010, thus we only use this subset of data in the air pollution dataset. 
#     
# # Data processing
# 
#     * I exclude countries with insufficinet number of record (Albania, Lichtenstein)
#     * I fill the missing values of remaining countries with avergae across years per country
#     * I use mean to aggregate the data

"""
Adapted from "original/Damian/exploratory_analysis_eurostat_diseases_deaths.ipynb" 
for the 'Eurostat' page in the Application
"""

import os
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

COUNTRIES_TO_DROP = ['Albania', 'Liechtenstein']
names_of_selected_groups_of_diseases = [
    'Certain infectious and parasitic diseases (A00-B99)',
    'Neoplasms',
    'Mental and behavioural disorders (F00-F99)',
    'Diseases of the nervous system and the sense organs (G00-H95)',
    'Diseases of the circulatory system (I00-I99)',
    'Diseases of the skin and subcutaneous tissue (L00-L99)',
    'Pregnancy, childbirth and the puerperium (O00-O99)',
    
]


# build dataframe of each disease from eurostat
def parse_sheet(workbook, sheet_name):
    lines_to_skip = 8
    list_to_drop = COUNTRIES_TO_DROP
    interim_xls = pd.read_excel(workbook, sheet_name, skiprows=lines_to_skip)
    name_of_disease = interim_xls.iloc[0, 1]
#     print(name_of_disease)
    final_xls = interim_xls.iloc[2:-3,:]
    final_xls = final_xls.set_index(final_xls.iloc[:,0])
    final_xls = final_xls.iloc[:,1:]
    final_xls = final_xls.replace(':', np.nan)
    final_xls.drop(labels=list_to_drop)
    header = final_xls.iloc[0]
    final_xls = final_xls.rename(columns=header)
    final_xls = final_xls.iloc[1:]
    final_xls_rown_mean = final_xls.mean(axis=0)
    final_xls.fillna(final_xls_rown_mean, inplace=True)
    return final_xls, name_of_disease


def preprocess_data():
    # Load the excel sheet for deaths caused by different diseases
    raw_data_dir = os.path.join('data', 'raw')
    path_to_health_data = os.path.join(raw_data_dir, 'eurostat_diseases', 'eurostat_disease_data.xls')
    health_xls = pd.ExcelFile(path_to_health_data)
    # Load the excel sheet containing exposure to PM2.5 and PM10 air polution in europe
    path_to_air_pollution_data = os.path.join('data', 'interim', 'eurostat_air_pollution.xlsx')
    pollution_xls = pd.ExcelFile(path_to_air_pollution_data)
    pollution_25_df = pd.read_excel(pollution_xls, 'Sheet 1')
    pollution_25_df = pollution_25_df.set_index(pollution_25_df.iloc[:,0])
    pollution_25_df = pollution_25_df.iloc[:,1:]

    # replacing the values of nan with row average
    pollution_25_df = pollution_25_df.replace(':', np.nan)
    pollution_25_df.fillna(pollution_25_df.mean(axis=0), axis=0, inplace=True)

    for (col_name, col_data) in pollution_25_df.iteritems():
        pollution_25_df[col_name] = pd.to_numeric(pollution_25_df[col_name])

    pollution_10_df = pd.read_excel(pollution_xls, 'Sheet 2')
    pollution_10_df = pollution_10_df.set_index(pollution_10_df.iloc[:,0])
    pollution_10_df = pollution_10_df.iloc[:,1:]
    pollution_10_df

    # replacing the values of nan with row average
    pollution_10_df = pollution_10_df.replace(':', np.nan)
    pollution_10_df.fillna(pollution_10_df.mean(axis=0), axis=0, inplace=True)

    for (col_name, col_data) in pollution_10_df.iteritems():
        pollution_10_df[col_name] = pd.to_numeric(pollution_10_df[col_name])

    # Building dataframe from health_xls
    total_deaths, name = parse_sheet(health_xls, 'Data')
    total_deaths.head(10)

    disease_names_list = []
    disease_name_data_dict = {}
    for sheetname in health_xls.sheet_names:
        # print(sheetname)
        # first sheets are for all age groups, later they are divided into groups whch is less interesting
        if sheetname != 'Data87':
            data, disease_name = parse_sheet(health_xls, sheetname)
            disease_names_list.append(disease_name)
            # print(disease_name)
            disease_name_data_dict[disease_name] = data
        else:
            break

    # Data for pm2.5 in years 2005-2010
    pollution_25_df_agg_year_avg = pollution_25_df.iloc[:,0:6].agg('mean', axis=1)
    pollution_25_df_agg_year_avg.rename(index={1:'AGE'})
    pollution_25_df_agg_year_avg.index.names = ['AGE']
    # Data for pm10
    pollution_10_df_agg_year_avg = pollution_10_df.iloc[:,1:11].agg('mean', axis=1)
    pollution_10_df_agg_year_avg.rename(index={1:'AGE'})
    pollution_10_df_agg_year_avg.index.names = ['AGE']
    # Pack into a tuple
    processed_data = (pollution_25_df_agg_year_avg,
                      pollution_10_df_agg_year_avg)

    return disease_name_data_dict, names_of_selected_groups_of_diseases, processed_data


def aggregate_disease_year_average(disease_dataset, disease_id):
    disease_df = disease_dataset[disease_id]
    disease_df_agg_year_avg = disease_df.agg('mean', axis=1)
    return disease_df_agg_year_avg


def inner_join(disease, pollution):
    common = pd.concat([disease, pollution], join='inner', axis=1)
    return common


# # This version of scatter_plot creates a NEW figure and returns it
# def scatter_plot(
#     disease_vs_pollution_dataframe,
#     title = None, 
#     y_label = 'Number of deaths per 100000 inhabitants', 
#     x_label = 'air pollution density', 
#     fig_size = (10, 6)):
#     fig, ax = plt.subplots(figsize = fig_size)
#     ax.scatter(x=disease_vs_pollution_dataframe.iloc[:,1], y=disease_vs_pollution_dataframe.iloc[:,0])
    
#     for i, txt in enumerate(disease_vs_pollution_dataframe.index):
#         ax.annotate(txt, (disease_vs_pollution_dataframe.iloc[i,1], disease_vs_pollution_dataframe.iloc[i,0]))
    
#     ax.set_xlabel(x_label)
#     ax.set_ylabel(y_label)
#     if title is not None:
#         ax.set_title(title)

#     return fig


# # This version of plot_disease returns the NEWLY created figure on which it was plotted
# def plot_disease(disease_name_data_dict, disease_id, pollution_X_df_agg_year_avg, pm_type, fig_size = (10, 6)):
#     disease_agg_year = aggregate_disease_year_average(disease_name_data_dict, disease_id)
#     disease_pollution_joined = inner_join(disease_agg_year, pollution_X_df_agg_year_avg)
#     fig = scatter_plot(
#         disease_pollution_joined,
#         title = disease_id,
#         x_label = 'Average {} concentration'.format(pm_type),
#         fig_size = fig_size)

#     return fig


# This version of the function plots on existing axes, so it can modify an EXISTING figure
def scatter_plot_axes(
    ax,
    disease_vs_pollution_dataframe,
    title = None, 
    y_label = 'Number of deaths per 100000 inhabitants', 
    x_label = 'air pollution density'):
    ax.scatter(x=disease_vs_pollution_dataframe.iloc[:,1], y=disease_vs_pollution_dataframe.iloc[:,0])
    
    for i, txt in enumerate(disease_vs_pollution_dataframe.index):
        ax.annotate(txt, (disease_vs_pollution_dataframe.iloc[i,1], disease_vs_pollution_dataframe.iloc[i,0]))
    
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    if title is not None:
        ax.set_title(title)


# This version of the function plots on existing axes, so it can modify an EXISTING figure
def plot_disease_axes(ax, disease_name_data_dict, disease_id, pollution_X_df_agg_year_avg, pm_type):
    disease_agg_year = aggregate_disease_year_average(disease_name_data_dict, disease_id)
    disease_pollution_joined = inner_join(disease_agg_year, pollution_X_df_agg_year_avg)
    scatter_plot_axes(
        ax,
        disease_pollution_joined,
        title = disease_id,
        x_label = 'Average {} concentration'.format(pm_type))
