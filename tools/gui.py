"""
File which defines the GUI for the Application.
Data handling and plotting is done in seperate files.

The application makes use of TkInter, the built-in library for simple python GUIs,
as well as the matplotlib backend for integrating plots with TkInter.

The main application is a TkInter root window (tk.Tk), while the seperate pages are frames (tk.Frame) 
They occupy the window and can be raised to the top.
"""

import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
from matplotlib import pyplot as plt
import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk

from tools import eurostat, medical

LARGE_FONT = ("Verdana", 12)
FIGURE_SIZE = (10, 6)
LOGO_SIZE = 300


# The actual window, which is also a container for the various pages (frames)
class Application(tk.Tk):
	def __init__(self, *args, **kwargs):
		tk.Tk.__init__(self, *args, **kwargs)
		tk.Tk.wm_title(self, "SMOG Analysis")
		tk.Tk.iconbitmap(self, "images/smog_logo.ico")
		# tk.Tk.wm_geometry(self, "800x800")

		container = tk.Frame(self)
		container.pack(side = "top", fill = "both", expand = True)
		container.grid_rowconfigure(0, weight = 1)
		container.grid_columnconfigure(0, weight = 1)

		self.frames = {}
		for F in (StartPage, Eurostat, Medical):
			frame = F(container, self)
			self.frames[F] = frame
			frame.grid(row = 0, column = 0, sticky = "nsew")

		self.show_frame(StartPage)

	def show_frame(self, F):
		frame = self.frames[F]
		frame.tkraise()


# The home page
class StartPage(tk.Frame):
	def __init__(self, parent, controller):
		tk.Frame.__init__(self, parent)
		# Introduction
		l1 = tk.Label(self, 
			text = "\nSMOG Analysis - Analyzing the Effect of Air Pollution", font = LARGE_FONT)
		l1.pack()
		l2 = tk.Label(self, 
			text = "Project for Advanced Python Programming", font = LARGE_FONT)
		l2.pack()
		l3 = tk.Label(self, 
			text = "Andrzej Putyra, Damian Kwaśny, Weronika Dranka, Magdalena Adamczyk, Diego Diaz\n")
		l3.pack()
		# Displaying Logo
		c_frame = tk.Frame(self)
		c_frame.pack()
		c = tk.Canvas(c_frame, width = LOGO_SIZE, height = LOGO_SIZE)
		c.pack()
		img = Image.open("images/smog_logo.png").resize((LOGO_SIZE, LOGO_SIZE))
		self.img = ImageTk.PhotoImage(img)
		c.create_image(0, 0, image = self.img, anchor = 'nw')
		# Buttons for navigating to other pages
		b1 = ttk.Button(self, text = "Eurostat", command = lambda: controller.show_frame(Eurostat))
		b1.pack()
		b2 = ttk.Button(self, text = "Medical", command = lambda: controller.show_frame(Medical))
		b2.pack()


# Page for displaying plots based on Eurostat data (handled by "tools/eurostat.py")
class Eurostat(tk.Frame):
	def __init__(self, parent, controller):
		tk.Frame.__init__(self,parent)
		# Description of dataset used and return button
		label = tk.Label(self, 
			text = "Eurostat - Exposure to Air Pollution by Particulate Matter in Countries", 
			font = LARGE_FONT)
		label.grid(row = 0, column = 0, columnspan = 4)
		b1 = ttk.Button(self, text = "Start Page", command = lambda: controller.show_frame(StartPage))
		b1.grid(row = 0, column = 4, sticky = 'e')

		# Gather preprocessed eurostat data
		(self.disease_dict, self.selected_disease_groups, 
		 	(self.pm25_yearly_avg, self.pm10_yearly_avg)) = eurostat.preprocess_data()
		# Choose disease group
		l1 = tk.Label(self, text = "Disease group:")
		l1.grid(row = 1, column = 0, sticky = 'e')
		cb1_choices = self.selected_disease_groups
		self.cb1 = ttk.Combobox(self, values = cb1_choices, state = "readonly", width = 60)
		self.cb1.grid(row = 1, column = 1, sticky = 'w')
		self.cb1.current(0)
		# Choose pollutant
		l2 = tk.Label(self, text = "Pollutant type:")
		l2.grid(row = 1, column = 2, sticky = 'e')
		cb2_choices = ["PM 2.5", "PM 10"]
		self.cb2 = ttk.Combobox(self, values = cb2_choices, state = "readonly")
		self.cb2.grid(row = 1, column = 3, sticky = 'w')
		self.cb2.current(0)
		# Button to plot figure according to chosen parameters
		plotButton = ttk.Button(self, text = "Plot figure", command = lambda: self.draw_figure())
		plotButton.grid(row = 1, column = 4, sticky = 'e')

		# Create a figure with its corresponding axes object
		self.fig, self.ax = plt.subplots(figsize = FIGURE_SIZE)
		# Create a frame for the canvas, the canvas, and display them
		# The frame is necessary for displaying it on a grid
		self.canvas_frame = tk.Frame(self)
		self.canvas_frame.grid(row = 2, column = 0, columnspan = 5)
		self.canvas = FigureCanvasTkAgg(self.fig, self.canvas_frame)
		self.canvas.draw()
		self.canvas.get_tk_widget().pack(side = "bottom", fill = "both", expand = True)
		# Create a frame for the plot's toolbar, the toolbar, and display it
		# The frame is necessary for displaying it on a grid
		self.toolbar_frame = tk.Frame(self)
		self.toolbar_frame.grid(row = 3, column = 0, columnspan = 5)
		self.toolbar = NavigationToolbar2Tk(self.canvas, self.toolbar_frame)
		self.canvas._tkcanvas.pack(side = "bottom", fill = "both", expand = True)
		self.toolbar.update()
		# Close figure, in case draw_figure never gets run self.fig will hold memory
		plt.close('all')


	def draw_figure(self):
		# Modify the axes of the page's figure with the new content
		self.ax.clear()
		if self.cb2.get() == "PM 2.5":
			eurostat.plot_disease_axes(self.ax, self.disease_dict, 
				self.cb1.get(), self.pm25_yearly_avg, "PM 2.5")
		elif self.cb2.get() == "PM 10":
			eurostat.plot_disease_axes(self.ax, self.disease_dict, 
				self.cb1.get(), self.pm10_yearly_avg, "PM 10")
		# Redraw the figure now that the axes are changed
		self.fig.canvas.draw()
		# Close all plots, otherwise references still exist in memory
		plt.close('all')


# Page for displaying plots based on Medical data (handled by "tools/medical.py")
class Medical(tk.Frame):
	def __init__(self, parent, controller):
		tk.Frame.__init__(self, parent)
		# Description of dataset used and return button
		label = tk.Label(self, 
			text = "Medical - Analysis of Effects on Health based on NFZ, Airly & GGIOS Data", 
			font = LARGE_FONT)
		label.grid(row = 0, column = 0, columnspan = 4)
		b1 = ttk.Button(self, text = "Start Page", command = lambda: controller.show_frame(StartPage))
		b1.grid(row = 0, column = 4, sticky = 'e')

		# Gather preprocessed medical data
		(self.preprocessed_data, self.countries) = medical.preprocess_data()
		# Choose country to plot
		l1 = tk.Label(self, text = "Country:")
		l1.grid(row = 1, column = 0, columnspan = 2, sticky = 'e')
		cb1_choices = self.countries
		self.cb1 = ttk.Combobox(self, values = cb1_choices, state = "readonly")
		self.cb1.grid(row = 1, column = 2, columnspan = 2, sticky = 'w')
		self.cb1.current(0)
		# Button to plot figure according to chosen parameters
		plotButton = ttk.Button(self, text = "Plot figure", command = lambda: self.draw_figure())
		plotButton.grid(row = 1, column = 4, sticky = 'e')

		# Create a figure with its corresponding axes object
		self.fig, self.ax = plt.subplots(figsize = FIGURE_SIZE)
		# Create a frame for the canvas, the canvas, and display them
		# The frame is necessary for displaying it on a grid
		self.canvas_frame = tk.Frame(self)
		self.canvas_frame.grid(row = 2, column = 0, columnspan = 5)
		self.canvas = FigureCanvasTkAgg(self.fig, self.canvas_frame)
		self.canvas.draw()
		self.canvas.get_tk_widget().pack(side = "bottom", fill = "both", expand = True)
		# Create a frame for the plot's toolbar, the toolbar, and display it
		# The frame is necessary for displaying it on a grid
		self.toolbar_frame = tk.Frame(self)
		self.toolbar_frame.grid(row = 3, column = 0, columnspan = 5)
		self.toolbar = NavigationToolbar2Tk(self.canvas, self.toolbar_frame)
		self.canvas._tkcanvas.pack(side = "bottom", fill = "both", expand = True)
		self.toolbar.update()
		# Close figure, in case draw_figure never gets run self.fig will hold memory
		plt.close('all')

	def draw_figure(self):
		# Modify the axes of the page's figure with the new content
		self.ax.clear()
		medical.plot_for_country_axes(self.ax, self.preprocessed_data, self.cb1.get())
		# Redraw the figure now that the axes are changed
		self.fig.canvas.draw()
		# Close all plots, otherwise references still exist in memory
		plt.close('all')
