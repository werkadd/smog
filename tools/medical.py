"""
Adapted from "original/Weronika/medical_smog_consequences.ipynb" 
for the 'Medical' page in the Application
"""

import pandas as pd
import numpy as np
import matplotlib
import json
from pandas.io.json import json_normalize
import scipy
import requests 
from scipy import signal
import matplotlib.pyplot as plt


def preprocess_data():
    # European data about health and air - https://ec.europa.eu/eurostat/web/sdi/good-health-and-well-being?p_p_id=NavTreeportletprod_WAR_NavTreeportletprod_INSTANCE_q65sCzKR7IGM&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-2&p_p_col_pos=2&p_p_col_count=4
    # Load raw data via pandas
    life_expectancy_at_birth_by_sex = pd.read_table('data/sdg_03_10.tsv', delimiter='\t')
    exposure_to_air_pollution_by_particulate_matter  = pd.read_table('data/sdg_11_50.tsv', delimiter='\t')
    share_of_people_with_good_perceived_health  = pd.read_table('data/sdg_03_20.tsv', delimiter='\t')
    death_rate_to_chronic_diseases  = pd.read_table('data/sdg_03_40.tsv', delimiter='\t')
    # Add indexes
    life_expectancy_at_birth_by_sex = life_expectancy_at_birth_by_sex.set_index('unit,sex,age,geo\\time')
    exposure_to_air_pollution_by_particulate_matter = exposure_to_air_pollution_by_particulate_matter.set_index('airpol,geo\\time')
    share_of_people_with_good_perceived_health = share_of_people_with_good_perceived_health.set_index('unit,quantile,age,sex,levels,geo\\time')
    death_rate_to_chronic_diseases = death_rate_to_chronic_diseases.set_index('sex,geo\\time')
    # Create tuple for processed data
    processed_data = (life_expectancy_at_birth_by_sex,
                      exposure_to_air_pollution_by_particulate_matter,
                      share_of_people_with_good_perceived_health,
                      death_rate_to_chronic_diseases)

    #get all countries abbreviations
    countries_abr = list(map(lambda x: x[-2:], share_of_people_with_good_perceived_health.index.values.tolist()))
    return processed_data, countries_abr


# # Create a NEW figure with the plot for requested country
# def plot_for_country(processed_data, country_abr, fig_size = (10, 6)):
#     # Unpack processed data
#     (life_expectancy_at_birth_by_sex,
#      exposure_to_air_pollution_by_particulate_matter,
#      share_of_people_with_good_perceived_health,
#      death_rate_to_chronic_diseases) = processed_data
    
#     # Restructure data into a valid frame for plotting
#     indexes_life = list(filter(lambda x: x.endswith(country_abr), life_expectancy_at_birth_by_sex.index.values.tolist()))
#     a = life_expectancy_at_birth_by_sex.loc[indexes_life,].T
#     a = a.applymap(lambda x: str(x).split(' ')[0])
    
#     indexes_exposure_air = list(filter(lambda x: x.endswith(country_abr), exposure_to_air_pollution_by_particulate_matter.index.values.tolist()))
#     b = exposure_to_air_pollution_by_particulate_matter.loc[indexes_exposure_air,].T
#     b = b.applymap(lambda x: str(x).split(' ')[0])
    
# #     indexes_good_perceived_health = list(filter(lambda x: x.endswith(country_abr), share_of_people_with_good_perceived_health.index.values.tolist()))
# #     c = share_of_people_with_good_perceived_health.loc[indexes_good_perceived_health,].T
# #     c = c.applymap(lambda x: str(x).split(' ')[0])
    
#     indexes_death = list(filter(lambda x: x.endswith(country_abr), death_rate_to_chronic_diseases.index.values.tolist()))
#     d = death_rate_to_chronic_diseases.loc[indexes_death,].T
#     d = d.applymap(lambda x: str(x).split(' ')[0])

#     joined = a.join(b)
#     joined = joined.join(d)

#     joined = joined.replace(": ", np.nan)
#     plot_data = joined.replace(":", np.nan)
#     for (columnName, columnData) in plot_data.iteritems():
#         plot_data[columnName] = pd.to_numeric(plot_data[columnName])

#     fig, ax = plt.subplots(figsize = fig_size)
#     plot_data.plot(ax = ax, title = country_abr)
#     return fig


# Modify EXISTING axes object with the data for the requested country's plot
def plot_for_country_axes(ax, processed_data, country_abr):
    # Unpack processed data
    (life_expectancy_at_birth_by_sex,
     exposure_to_air_pollution_by_particulate_matter,
     share_of_people_with_good_perceived_health,
     death_rate_to_chronic_diseases) = processed_data
    
    # Restructure data into a valid frame for plotting
    indexes_life = list(filter(lambda x: x.endswith(country_abr), life_expectancy_at_birth_by_sex.index.values.tolist()))
    a = life_expectancy_at_birth_by_sex.loc[indexes_life,].T
    a = a.applymap(lambda x: str(x).split(' ')[0])
    
    indexes_exposure_air = list(filter(lambda x: x.endswith(country_abr), exposure_to_air_pollution_by_particulate_matter.index.values.tolist()))
    b = exposure_to_air_pollution_by_particulate_matter.loc[indexes_exposure_air,].T
    b = b.applymap(lambda x: str(x).split(' ')[0])
    
#     indexes_good_perceived_health = list(filter(lambda x: x.endswith(country_abr), share_of_people_with_good_perceived_health.index.values.tolist()))
#     c = share_of_people_with_good_perceived_health.loc[indexes_good_perceived_health,].T
#     c = c.applymap(lambda x: str(x).split(' ')[0])
    
    indexes_death = list(filter(lambda x: x.endswith(country_abr), death_rate_to_chronic_diseases.index.values.tolist()))
    d = death_rate_to_chronic_diseases.loc[indexes_death,].T
    d = d.applymap(lambda x: str(x).split(' ')[0])

    joined = a.join(b)
    joined = joined.join(d)
    joined = joined.replace(": ", np.nan)
    plot_data = joined.replace(":", np.nan)
    for (columnName, columnData) in plot_data.iteritems():
        plot_data[columnName] = pd.to_numeric(plot_data[columnName])
    plot_data.plot(ax = ax, title = country_abr)
